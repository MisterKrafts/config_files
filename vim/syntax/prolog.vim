" Vim syntax file
" Language:    PROLOG
" Maintainer:  Anton Kochkov <anton.kochkov@gmail.com>
" Last Change: 2019 Aug 29

" There are two sets of highlighting in here:
" If the "prolog_highlighting_clean" variable exists, it is rather sparse.
" Otherwise you get more highlighting.
"
" You can also set the "prolog_highlighting_no_keyword" variable. If set,
" keywords will not be highlighted.

" quit when a syntax file was already loaded
if exists("b:current_syntax")
  finish
endif

" Prolog is case sensitive.
syn case match

" Very simple highlighting for comments, clause heads and
" character codes.  It respects prolog strings and atoms.

syn region   prologCComment start=+\v\/\*+ end=+\v\*\/+
syn match    prologComment  +%.*+

if !exists("prolog_highlighting_no_keyword")
  syn keyword  prologKeyword  module meta_predicate multifile dynamic
endif
syn match    prologCharCode +0'\\\=.+
syn region   prologString   start=+"+ skip=+\\\\\|\\"+ end=+"+
syn region   prologAtom     start=+'+ skip=+\\\\\|\\'+ end=+'+
syn match    prologFunctor  +\s*[a-z]\w*[(/]+ contains=prologPar,prologAtom2,prologArOp,prologKeyword,prologModule
syn match    prologModule   +\s\(\|use_\)module([a-z]\w*[,)]+ contains=prologPar,prologKeyword
syn region   prologClause   matchgroup=prologClauseHead start=+^[a-z]\w*+ matchgroup=Normal  end=+\.+ contains=ALLBUT,prologClause
syn match    prologPar      +\v[\(/\),]+
syn match    prologAtom2    +\v[ \t,\(][a-z][A-Za-z0-9_]*[ \t,\)]+ contains=prologPar,prologKeyword

if !exists("prolog_highlighting_clean")

  " some keywords
  " some common predicates are also highlighted as keywords
  " is there a better solution?
  if !exists("prolog_highlighting_no_keyword")
    syn keyword prologKeyword   abolish current_output  peek_code
    syn keyword prologKeyword   append  current_predicate       put_byte
    syn keyword prologKeyword   arg     current_prolog_flag     put_char
    syn keyword prologKeyword   asserta fail    put_code assert
    syn keyword prologKeyword   assertz findall read
    syn keyword prologKeyword   at_end_of_stream        float   read_term
    syn keyword prologKeyword   atom    flush_output    repeat
    syn keyword prologKeyword   atom_chars      functor retract
    syn keyword prologKeyword   atom_codes      get_byte        set_input
    syn keyword prologKeyword   atom_concat     get_char        set_output
    syn keyword prologKeyword   atom_length     get_code        set_prolog_flag
    syn keyword prologKeyword   atomic  halt    set_stream_position
    syn keyword prologKeyword   bagof   integer setof
    syn keyword prologKeyword   call    is      stream_property
    syn keyword prologKeyword   catch   nl      sub_atom use_module
    syn keyword prologKeyword   char_code       nonvar  throw
    syn keyword prologKeyword   char_conversion number  true
    syn keyword prologKeyword   clause  number_chars    unify_with_occurs_check
    syn keyword prologKeyword   close   number_codes    var
    syn keyword prologKeyword   compound        once    write
    syn keyword prologKeyword   copy_term       op      write_canonical
    syn keyword prologKeyword   current_char_conversion open    write_term
    syn keyword prologKeyword   current_input   peek_byte       writeq
    syn keyword prologKeyword   current_op      peek_char       dynamic
    syn keyword prologKeyword   nb_setval       nb_getval       nb_delete
    syn keyword prologKeyword   b_setval        b_getval
  endif

  syn match   prologArOp	      +\v[+*\-/]+ contains=prologCComment
  syn match   prologOperator 	      "=\\=\|=:=\|\\==\|=<\|==\|>=\|\\=\|\\+\|=\.\.\|<\|>\|="
  syn match   prologAsIs              "===\|\\===\|<=\|=>"

  syn match   prologNumber            "\<\d*\>'\@!"
  syn match   prologNumber            "\<0[xX]\x*\>'\@!"
  syn match   prologCommentError      "\*/"
  syn match   prologSpecialCharacter  ";"
  syn match   prologSpecialCharacter  "!"
  syn match   prologSpecialCharacter  ":-"
  syn match   prologSpecialCharacter  "-->"
  syn match   prologQuestion          "?-.*\."  contains=prologNumber


endif

syn sync maxlines=50


" Define the default highlighting.
" Only when an item doesn't have highlighting yet

" The default highlighting.
hi prologComment     		   ctermfg=White
hi prologCComment         	   ctermfg=White
hi def link prologCharCode         Special

if exists ("prolog_highlighting_clean")

hi def link prologKeyword        Statement
hi def link prologClauseHead     Statement
hi def link prologClause Normal

else

hi def link prologKeyword        Keyword
hi prologClauseHead 		 ctermfg=Magenta
hi prologFunctor 		 ctermfg=DarkCyan
hi prologModule 		 ctermfg=DarkRed
hi def link prologClause 	 Normal
hi def link prologQuestion       PreProc
hi prologSpecialCharacter	 ctermfg=Red
hi prologNumber 		 ctermfg=Brown
hi def link prologAsIs           Normal
hi def link prologCommentError   Error
hi prologAtom 			 ctermfg=LightBlue
hi prologAtom2			 ctermfg=LightBlue
hi def link prologString         String
hi def link prologOperator       Operator
hi def link prologPar		 Normal
hi prologArOp			 ctermfg=Brown

endif


let b:current_syntax = "prolog"

" vim: ts=8
