" my filetype file
if exists("did_load_filetypes")
  finish
endif
augroup filetypedetect
  au! BufRead,BufNewFile *.pl setfiletype prolog
  au! BufRead,BufNewFile *.plt setfiletype prolog
augroup END
