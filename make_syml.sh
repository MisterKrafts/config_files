if [ -d "~/.bash" ];
then
    mkdir ~/.bash
fi ;
rm -rf ~/.bash/aliases ;
ln -sf ~/.fichier_configuration/config_files/bash/aliases ~/.bash/aliases ;
rm -rf ~/.bash_profile ;
ln -sf ~/.fichier_configuration/config_files/bash_profile ~/.bash_profile ;
rm -rf ~/.emacs ;
ln -sf ~/.fichier_configuration/config_files/emacs ~/.emacs ;
rm -rf ~/.gitconfig ;
ln -sf ~/.fichier_configuration/config_files/gitconfig ~/.gitconfig ;
rm -rf ~/.nanorc ;
ln -sf ~/.fichier_configuration/config_files/nanorc ~/.nanorc ;
rm -rf ~/.ssh/config ;
ln -sf ~/.fichier_configuration/config_files/ssh/config ~/.ssh/config ;
rm -rf ~/.ssh/my_conf ;
ln -sf ~/.fichier_configuration/config_files/ssh/my_conf ~/.ssh/my_conf ;
rm -rf ~/.umaskrc ;
ln -sf ~/.fichier_configuration/config_files/umaskrc ~/.umaskrc ;
rm -rf ~/.vim;
ln -sf ~/.fichier_configuration/config_files/vim  ~/.vim ;
rm -rf ~/.virmc ;
ln -sf ~/.fichier_configuration/config_files/vimrc ~/.vimrc ;
